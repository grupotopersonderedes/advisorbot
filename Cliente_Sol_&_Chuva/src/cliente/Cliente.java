/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author lLuis Felipe
 */
public class Cliente implements Runnable {

    Socket cliente;

    public Cliente(Socket cliente) {
        this.cliente = cliente;
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        String ip = "localhost";
        int port = 76;
        Socket cliSocket = new Socket(InetAddress.getByName(ip), port);
        Cliente c = new Cliente(cliSocket);
        Thread t = new Thread(c);
        t.start();
    }

    @Override
    public void run() {
        Scanner scan = new Scanner(System.in);
        String comando;

        System.out.println("--------------------------------------------------------");
        System.out.println("Bem-vindo ao Sol & Chuva!!\n O que deseja fazer? ");
        System.out.println("Digite \"/comandos\" para listar os comando disponíveis!");
        comando = scan.nextLine();
        try {

            switch (comando) {
                case "/climaagora":
                    String cidade,
                     uf;
                    System.out.println("--------------------------------------------------------");
                    System.out.println("Qual cidade deseja consultar?");
                    cidade = scan.nextLine();
                    scan = new Scanner(System.in);
                    System.out.println("Qual o estado?");
                    uf = scan.nextLine();
                    requestClimaAgora(cidade, uf);
                    break;
                case "/rndclimaagora":
                    commonRequests(comando);
                    break;
                case "/autores":
                    commonRequests(comando);
                    break;
                case "/comandos":
                    commonRequests(comando);
                    break;
                default:
                    System.out.println("Comando errado, tente novamente!");
                    break;
            }
        } catch (IOException ex) {
            ex.getStackTrace();
            ex.getMessage();
        }

    }

    public void commonRequests(String comando) throws IOException {
        PrintWriter pw = new PrintWriter(cliente.getOutputStream());
        pw.print("" + comando + "\n");
        pw.flush();
        processaResposta(cliente);
    }

    public void requestClimaAgora(String cidade, String uf) throws IOException {
        PrintWriter pw = new PrintWriter(cliente.getOutputStream());
        pw.print("/climaagora\n");
        pw.print("" + cidade + "\n");
        pw.print("" + uf + "\n");
        pw.flush();
        processaResposta(cliente);

    }

    public void processaResposta(Socket cliente) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
        String t;
        System.out.println("--------------------------------------------------------");
        while ((t = br.readLine()) != null) {
            System.out.println(t);
        }
        br.close();
        System.out.println("Conexão encerrada");
    }
}
