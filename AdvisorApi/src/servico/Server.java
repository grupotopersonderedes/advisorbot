/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servico;

/**
 *
 * @author 151150945
 */
import controller_api.AdvisorApi;
import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cidade; 
import model.Clima;

//import org.zeromq.SocketType;
//import org.zeromq.ZMQ;
//import org.zeromq.ZContext;
//
//  Weather update server in Java
//  Binds PUB socket to tcp://*:5556
//  Publishes random weather updates
//
public class Server implements Runnable {

    Socket cliente;

    public Server(Socket cliente) {
        this.cliente = cliente;
    }

    public static void main(String[] args) {
        try {
            ServerSocket servidor = new ServerSocket(76);
            System.out.println("Servidor ouvindo na porta 76");
            while (true) {
                Socket cliente = servidor.accept();
                // Cria uma thread do servidor para tratar a conexão
                Server tratamento = new Server(cliente);
                Thread t = new Thread(tratamento);
                // Inicia a thread para o cliente conectado
                t.start();
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        String comando;
        AdvisorApi api;
        Cidade cidade;
        Clima clima;
        String nomeCidade, uf;

        api = new AdvisorApi();
        try {
            System.out.println("Cliente conectado: " + cliente.getInetAddress().getHostAddress());
            BufferedReader bufferEntrada = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
            PrintWriter pw = new PrintWriter(cliente.getOutputStream());

            while ((comando = bufferEntrada.readLine()) != null) {
//                
                switch (comando) {
                    case "/climaagora":
                        nomeCidade = bufferEntrada.readLine();
                        uf = bufferEntrada.readLine();
                        cidade = api.buscaCidadePorNome(nomeCidade, uf);
                        clima = api.climaAgora(cidade.getId());
                        printClima(pw, clima);
                        break;
                    case "/comandos":
                        pw.print("autores : /autores\n");
                        pw.print("comandos : /comandos\n");
                        pw.print("climaagora : /climaagora\n");
                        pw.print("rndclimaagora : /rndclimaagora\n");
                        pw.flush();
                        pw.close();
                        break;
                    case "/autores":
                        pw.print("autor01 : Luis Alves\n");
                        pw.print("autor02 : Diovane Freitas\n");
                        pw.print("autor03 : William Getsberg\n");
                        pw.flush();
                        pw.close();
                        break;
                    case "/rndclimaagora":
                        int randomId;
                        randomId = (int) (Math.random() * (6000 - 5000) + 5000);
                        clima = api.climaAgora(randomId + "");
                        printClima(pw, clima);
                        break;
                }
            }

        } catch (IOException ex) {
            desconectaCliente(cliente);
            System.out.println("Cliente Desconectado");
        } catch (Throwable ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void printClima(PrintWriter pw, Clima clima) {
        pw.print("cidade : " + clima.getCidade() + "\n");
        pw.print("temperatura : " + clima.getTemperatura() + "\n");
        pw.print("Sensação Termica : " + clima.getSensacao() + "\n");
        pw.print("Velocidade do Vento : " + clima.getVelocVento() + "\n");
        pw.print("Direção do Vento : " + clima.getDirecVento() + "\n");
        pw.print("Humidade Relativa do Ar : " + clima.getHumidadeAr() + "\n");
        pw.print("Condição : " + clima.getCondicao() + "\n");
        pw.print("Pressão Atmosférica : " + clima.getPressao() + "\n");
        pw.flush();
        pw.close();
    }

    public void desconectaCliente(Socket cliente) {
        try {
            cliente.close();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
