/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Luis Felipe
 */
public class Clima {

    private final String temperatura;
    private final String umidadeAr;
    private final String direcVento;
    private final String velocVento;
    private final String condicao;
    private final String pressao;
    private final String sensacao;
    private String cidade;

    public Clima(String temp, String humid, String direcVento, String velocVento,
            String condicao, String pressao, String sensacao, String cidade) {

        this.temperatura = temp;
        this.umidadeAr = humid;
        this.direcVento = direcVento;
        this.velocVento = velocVento;
        this.condicao = condicao;
        this.pressao = pressao;
        this.sensacao = sensacao;
        this.cidade = cidade;
    }

    public String getTemperatura() {
        return this.temperatura;
    }

    public String getHumidadeAr() {
        return this.umidadeAr;
    }

    public String getDirecVento() {
        return this.direcVento;
    }

    public String getVelocVento() {
        return this.velocVento;
    }

    public String getCondicao() {
        return this.condicao;
    }

    public String getPressao() {
        return this.pressao;
    }

    public String getSensacao() {
        return this.sensacao;
    }
    
    public String getCidade() {
        return this.cidade;
    }
}
