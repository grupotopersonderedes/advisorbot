/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Luis Felipe
 */
public class Cidade {
    
    private String nomeCidade;
    private Estado estado;
    private String id;
    
    public Cidade(String nome, String uf, String id){
        
        this.nomeCidade = nome;
        this.estado = new Estado(uf);
        this.id = id;
    }

    public String getNomeCidade() {
        return nomeCidade;
    }

    public Estado getEstado() {
        return estado;
    }
    
    public String getId() {
        return this.id;
    }
    
}
