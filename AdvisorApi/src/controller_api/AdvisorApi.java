/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller_api;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Cidade;
import model.Clima;

/**
 *
 * @author Luis Felipe
 */
public class AdvisorApi extends ApiClima {

// /api/v1/locale/city?name=São Paulo&state=SP&token=
    private Cidade cidade;
    final String TOKEN = "5af63bdf53b55b49dbbdc7ed2a39f01e";
    final String HOST = "apiadvisor.climatempo.com.br";
    final int PORT = 80;
    private Socket socket;
    private PrintWriter pw;
    private BufferedReader buffer;
    private String jsonString;
    private JsonObject resposta;
    private JsonObject respostaClima;
    private Clima clima;

    /**
     * Realiza uma chamada a Api utilizando o ip da cidade para consultar as
     * informações de clima relacionadas.
     *
     *
     * @param id
     * @return
     */
    @Override
    public Clima climaAgora(String id) {
        String temperatura;
        String umidade;
        String direcaoVento;
        String velocidade;
        String condicao;
        String pressao;
        String sensacao;
        String nomeCidade;
        try {

            setSocket();
            // O trecho abaixo é responsável por montar o cabeçalho da requisição a ser enviada.
            pw.print("GET /api/v1/weather/locale/" + id + "/current?token=" + TOKEN + " HTTP/1.1\r\n");
            pw.print("Host: " + HOST + "\r\n\r\n");
            pw.print("");
            pw.flush();

            // O trecho abaixo inicializa o buffer, chama o metodo que percorre a resposta e realiza o parsing do json
            buffer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            jsonString = retornaJsonString(buffer);
            resposta = new JsonParser().parse(jsonString).getAsJsonObject();

            respostaClima = resposta.get("data").getAsJsonObject();
            // Manipulação do JSon para recuperação dos dados de clima.
            temperatura = respostaClima.get("temperature").getAsString();
            umidade = respostaClima.get("humidity").getAsString();
            direcaoVento = respostaClima.get("wind_direction").getAsString();
            velocidade = respostaClima.get("wind_velocity").getAsString();
            condicao = respostaClima.get("condition").getAsString();
            pressao = respostaClima.get("pressure").getAsString();
            nomeCidade = resposta.get("name").getAsString();
            try {
                sensacao = respostaClima.get("sensation").getAsString();
            } catch (UnsupportedOperationException ex) {
                sensacao = "Sem informação";
            }

            clima = new Clima(temperatura, umidade, direcaoVento, velocidade, condicao, pressao, sensacao, nomeCidade);
            return clima;
        } catch (IOException ex) {
            Logger.getLogger(AdvisorApi.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NullPointerException ex) {

        }

        return null;
    }

    /**
     * Realiza uma chamada a api para buscar informações da cidade selecionada
     * Então, monta e o objeto da cidade. Essa busca é realizada para poder
     * termos acesso ao ID da cidade.
     *
     * @param nome
     * @param uf
     * @return
     */
    @Override
    public Cidade buscaCidadePorNome(String nome, String uf) {
        String id, name, state;
        nome = nome.replace(" ", "+");
        try {

            setSocket();
            // O trecho abaixo é responsável por montar o cabeçalho da requisição a ser enviada.
            pw.print("GET /api/v1/locale/city?name=" + nome + "&state=" + uf + "&token=" + TOKEN + " HTTP/1.1\r\n");
            pw.print("Host: " + HOST + "\r\n\r\n");
            pw.print("");
            pw.flush();

            // O trecho abaixo inicializa o buffer, chama o metodo que percorre a resposta e realiza o parsing do json
            buffer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            jsonString = retornaJsonString(buffer);
            jsonString = jsonString.substring(1, jsonString.length() - 1);
            resposta = new JsonParser().parse(jsonString).getAsJsonObject();

            // Manipulação do JSon para recuperação dos dados de clima.
            id = resposta.get("id").getAsString();
            name = resposta.get("name").getAsString();
            state = resposta.get("state").getAsString();
            cidade = new Cidade(name, state, id);
            return cidade;
        } catch (IOException ex) {
            Logger.getLogger(AdvisorApi.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Percorre o buffer passado por parâmetro ignorando as demais linhas da
     * resposta da requisição e pegando apenas a linha que contém o json.
     *
     * @param buffer
     * @return
     * @throws IOException
     */
    private String retornaJsonString(BufferedReader buffer) throws IOException {
        for (int i = 0; i < 14; i++) {
            if (i == 13) {
                return buffer.readLine();

            } else {
                buffer.readLine();
            }
        }
        return null;
    }

    /**
     * Inicializa a varíavel goblal do socket efetuando a conexão com o host
     * Inicializa também através do socket a printWriter responsável por montar
     * o cabeçalho da requisição
     *
     * @throws UnknownHostException
     * @throws IOException
     */
    private void setSocket() throws UnknownHostException, IOException {
        socket = new Socket(InetAddress.getByName(HOST), PORT);
        pw = new PrintWriter(socket.getOutputStream());
    }

}
