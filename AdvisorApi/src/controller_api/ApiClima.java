/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller_api;

import model.Cidade;
import model.Clima;

/**
 *
 * @author liped
 */
public abstract class ApiClima  {
    
    public abstract Clima climaAgora(String id);
    public abstract Cidade buscaCidadePorNome(String cityName, String uf);
    
}
